class Material
  include Mongoid::Document
  include Mongoid::Paperclip
  belongs_to :user
  field :file
  field :user_id
  has_mongoid_attached_file :file
  validates :file, presence: true
  validates_attachment_content_type :file, content_type: %w(image/jpeg image/jpg image/png application/pdf)
end