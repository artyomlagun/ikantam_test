class MaterialsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @materials =  Material.order('id DESC').all
  end

  def new
    @material = Material.new
  end

  def create
    @material = Material.new(material_params)

    if @material.save
      redirect_to materials_path
    else
      render :new
    end
  end

  def show
    @material = Material.find(params[:id])
  end

  private
  def material_params
    params.require(:material).permit(:file, :user_id)
  end
end
